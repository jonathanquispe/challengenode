angular.module('todoService', [])

	// super simple service
	// each function returns a promise object 
	.factory('Todos', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/users');
			},
			getUser : function(id) {
				return $http.get('/api/users/' + id);
			},
			create : function(todoData) {
				return $http.post('/api/user', todoData);
			},
			update : function(todoData) {
				return $http.put('/api/user/'+ todoData["_id"], todoData);
			},
			delete : function(id) {
				return $http.delete('/api/user/' + id);
			}
		}
	}]);