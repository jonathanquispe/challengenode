angular.module('todoController', [])

	// inject the Todo service factory into our controller
	.controller('mainController', ['$scope','$http','Todos', function($scope, $http, Todos) {
		$scope.formData = {};
		$scope.loading = true;
		$scope.user = {};

		var socket = io();
		socket.on('new conn', function(msg){			
        	angular.element( document.querySelector('#messages')).append('<li class="list-group-item">'+msg+'<li>');
        	Todos.get()
			.success(function(data) {				
				$scope.todos = data;
				$scope.loading = false;
			});
      	});
      	
		Todos.get()
			.success(function(data) {				
				$scope.todos = data;
				$scope.loading = false;
			});

		// CREATE ==================================================================
		// when submitting the add form, send the text to the node API
		$scope.createTodo = function() {
			$scope.loading = true;			
			if ($scope.formData.userName != undefined) {
				//socket.emit('chat message', $scope.formData.text);
				Todos.create($scope.formData)
					.success(function(data) {
						$scope.loading = false;
						$scope.formData = {}; // clear the form so our user is ready to enter another
						$scope.todos = data; // assign our new list of todos						    
					});
			}
		};

		$scope.deleteTodo = function(id,user) {
			$scope.loading = true;
			Todos.delete(id)
				.success(function(data) {
					$scope.loading = false;
					$scope.todos = data;						
				});
		};

		$scope.loadUser = function(id) {
			$scope.loading = true;
			Todos.getUser(id)
				.success(function(data) {
					$scope.loading = false;
					$scope.user = data;
					console.log("load user");
					console.log(data);					
				});
		};

		$scope.updateUser = function() {
			$scope.loading = true;
			Todos.update($scope.user)
				.success(function(data) {
					$scope.loading = false;
					$scope.todos = data;			
				});
		};
		

		

	}]);