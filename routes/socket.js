module.exports = function(io,mongodb) {
    var message={
         connected : "[{ip}] has connected",
         disconnected : "[{ip}] has disconnected",
         create : "[{ip}] added new user {msg}",
         update : "[{ip}] update user {msg}",
         delete : "[{ip}] delete new user {msg}",
         search : "[{ip}] perform search with filter {msg}"         
    };
    io.on('connection', function(socket){  
      io.emit('new conn', message["connected"].replace("{ip}", socket.client.conn.remoteAddress)); 
      socket.on('disconnect', function() {
           io.emit('new conn', message["disconnected"].replace("{ip}", socket.client.conn.remoteAddress));             
      });     
    });

};
